package ddddddaa.aaassdd;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.avos.avoscloud.AVException;
import com.avos.avoscloud.AVObject;
import com.avos.avoscloud.AVQuery;
import com.avos.avoscloud.GetCallback;
import com.avos.avoscloud.SaveCallback;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private AVObject avObject;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AVQuery<AVObject> showAD = new AVQuery<>("showAD");
        AVQuery<AVObject> appPackage = showAD.whereEqualTo("appPackage", AppUtils.getPackageName(this));
        appPackage.getFirstInBackground(new GetCallback<AVObject>() {
            @Override
            public void done(AVObject object, AVException e) {
                if (object == null) {
                    addObj();
                } else {
                    avObject = object;
                    getObjData();
                }
            }
        });
    }
    private void addObj() {
        AVObject appObject = new AVObject("showAD");
        appObject.put("domainUrl", "");
        appObject.put("if_show", false);
        appObject.put("imgUrl", "");
        appObject.put("innerAD1", "");
        appObject.put("innerAD2", "");
        appObject.put("innerAD3", "");
        appObject.put("innerAD4", "");
        appObject.put("appName", AppUtils.getAppName(this));
        appObject.put("appPackage", AppUtils.getPackageName(this));
        avObject = appObject;
        appObject.saveInBackground(new SaveCallback() {
            @Override
            public void done(AVException e) {
                LogUtil.e("添加成功");
                //做加载操作
                getObjData();
            }
        });
        LogUtil.e(AppUtils.getPackageName(this));
    }
    private void getObjData() {
        boolean ifShow = avObject.getBoolean("if_show");
        if (ifShow) {
            String domainUrl = avObject.getString("domainUrl");
            String imgUrl = avObject.getString("imgUrl");
            Intent intent = new Intent(this,BannerShowWebActivity.class);
            intent.putExtra("url",domainUrl);
            startActivity(intent);
            LogUtil.e(domainUrl + "\t" + imgUrl);
        } else {
            LogUtil.e("不显示");
        }

    }

}
